///////////////////////////////////////////////////////////////////////////////
//
//  Simple Web Service
//  ==================
//
// A very simple restful WebService, which responds to a HTTP GET request
// with 'Hello'.
//
///////////////////////////////////////////////////////////////////////////////

// Imports /////

const express = require('express');
const cors    = require('cors');


// Server /////

const app = express();
app.use(cors());


// Routes /////

app.get('/hello', function (req, res) {
  res.send('Hello');
});


// Help /////

app.listen(3100, function () {
  console.log(`** * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
**  
** The server is running on port 3100!
**
** You can open a browser window and perform the request
** 
**   http://localhost:3100/hello
**
** The server will send 'Hello' as response.
**
** Stop the server by pressing Ctrl+C.
**
** * * * * * * * * * * * * * * * * * * * * * * * * * * * * *`);
});
